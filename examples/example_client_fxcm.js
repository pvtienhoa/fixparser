import FIXParser, {
    Field,
    Fields,
    Messages,
    Side,
    OrderTypes,
    HandlInst,
    TimeInForce,
    EncryptMethod
} from '../src/FIXParser'; // from 'fixparser';
import { BeginString } from '../src/constants/ConstantsField';

const fixParser = new FIXParser();
const SENDER = 'MD_D103289979_client1';
const TARGET = 'FXCM';

function sendLogon() {
    const logon = fixParser.createMessage(
        new Field(Fields.MsgType, Messages.Logon),
        new Field(Fields.BeginString, 'FIX.4.4'),
        new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
        new Field(Fields.SenderCompID, SENDER),
        new Field(Fields.SendingTime, fixParser.getTimestamp()),
        new Field(Fields.TargetCompID, TARGET),
        new Field(Fields.TargetSubID, 'U100D1'),
        new Field(Fields.Username, 'D103289979'),
        new Field(Fields.Password, '5020'),
        new Field(Fields.ResetSeqNumFlag, 'Y'),
        new Field(Fields.EncryptMethod, EncryptMethod.None),
        new Field(Fields.HeartBtInt, 30)
    );
    const messages = fixParser.parse(logon.encode());
    console.log('sending message', messages[0].description, messages[0].string);
    fixParser.send(logon);
}

fixParser.connect({ host: 'fixdemo.fxcorporate.com', port: 9001, protocol: 'tcp', sender: SENDER, target: TARGET, fixVersion: 'FIX.4.4', targetSubID: 'U100D1', userName: 'D103289979', password: '5020'});

fixParser.on('open', () => {
    console.log('Open');

    sendLogon();

    // setInterval(() => {
    //     const order = fixParser.createMessage(
    //         new Field(Fields.MsgType, Messages.NewOrderSingle),
    //         new Field(Fields.MsgSeqNum, fixParser.getNextTargetMsgSeqNum()),
    //         new Field(Fields.SenderCompID, SENDER),
    //         new Field(Fields.SendingTime, fixParser.getTimestamp()),
    //         new Field(Fields.TargetCompID, TARGET),
    //         new Field(Fields.ClOrdID, '11223344'),
    //         new Field(Fields.HandlInst, HandlInst.AutomatedExecutionNoIntervention),
    //         new Field(Fields.OrderQty, '123'),
    //         new Field(Fields.TransactTime, fixParser.getTimestamp()),
    //         new Field(Fields.OrdType, OrderTypes.Market),
    //         new Field(Fields.Side, Side.Buy),
    //         new Field(Fields.Symbol, '700.HK'),
    //         new Field(Fields.TimeInForce, TimeInForce.Day)
    //     );
    //     const messages = fixParser.parse(order.encode());
    //     console.log('sending message', messages[0].description, messages[0].string.replace(/\x01/g, '|'));
    //     fixParser.send(order);
    // }, 500);

});
fixParser.on('message', (message) => {
    console.log('received message', message.description, message.string);
});
fixParser.on('close', () => {
    console.log('Disconnected');
});
